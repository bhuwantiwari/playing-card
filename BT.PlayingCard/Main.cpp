#include <iostream>
#include <conio.h>
#include <iomanip>
using namespace std;

//Making suits
enum Suit {
	HEARTS,
	SPADES,
	DIAMOND,
	CLUBS
};
//Making ranks
enum Rank {
	Two = 2,
	Three,
	Four,
	Five,
	Six, 
	Seven,
	Eight,
	Nine,
	Ten,
	Jack, 
	Queen,
	King, 
	ACE
};
//Combineing Ranks and Suits
struct Card {
	Suit Suit;
	Rank Rank;
};

int main() {
	Card c1;
	c1.Rank = Ten;

	Card c2;
	c2.Rank = ACE;

	if (c2.Rank > c1.Rank) cout << "c2 is higher";
	(void)_getch();
	return 0;
}